DataHandler
=========

DataHandler is a toolkit for realtime processing data.

Install
-------

First make sure you have installed the latest version of [node.js](http://nodejs.org)
(You may need to restart your computer after this step).

From NPM:

    mpm install @prof-itgroup/data-handler

Example
-------

For example you can complete the next task:

Incoming data

    x - Number
    y - Number

Expects

    x * x +  y / 2

```typescript
import { IDataConvertModel } from '@prof-itgroup/data-converter/lib/types';
import { IChainConfig, IHandlerConfig } from '@prof-itgroup/data-handler/types';
import { buildChain } from '@prof-itgroup/data-handler';

const handlers: IHandlerConfig[] = [
  {
    id    : 'sum',
    name  : 'sum',
    input : [
      { key: 'value1', required: true, defaultValue: undefined, nullable: false },
      { key: 'value2', required: true, defaultValue: undefined, nullable: false },
      { key: 'divider', required: false, defaultValue: 2, nullable: false },
    ],
    output: [
      { key: 'result', required: true, defaultValue: 0, nullable: false },
    ],
    code  : 'return { result: data.value1 + data.value2 };'
  },
  {
    id    : 'div',
    name  : 'div',
    input : [
      { key: 'dividend', required: true, defaultValue: undefined, nullable: false },
      { key: 'divider', required: true, defaultValue: undefined, nullable: false },
    ],
    output: [
      { key: 'result', required: true, defaultValue: 0, nullable: false },
    ],
    code  : 'return { result: data.dividend / data.divider };'
  },
  {
    id    : 'mul',
    name  : 'mul',
    input : [
      { key: 'value1', required: true, defaultValue: undefined, nullable: false },
      { key: 'value2', required: true, defaultValue: undefined, nullable: false },
    ],
    output: [
      { key: 'result', required: true, defaultValue: 0, nullable: false },
    ],
    code  : 'return { result: data.value1 * data.value2 };'
  },
];

const chain: IChainConfig = {
  id    : 'test',
  name  : 'test',
  input : [
    { key: 'x', required: true, nullable: false, defaultValue: undefined },
    { key: 'y', required: true, nullable: false, defaultValue: undefined },
  ],
  output: [
    { key: 'result', sourceKey: '3.result', nullable: false, defaultValue: undefined },
  ],
  links : [
    {
      id          : '1',
      handler_id  : 'mul',
      input       : [
        { key: 'value1', sourceKey: 'x', sourceRoot: 'input' },
        { key: 'value2', sourceKey: 'x', sourceRoot: 'input' },
      ],
      dependencies: [],
    },
    {
      id          : '2',
      handler_id  : 'div',
      input       : [
        { key: 'dividend', sourceKey: 'y', sourceRoot: 'input' },
        { key: 'divider', sourceKey: 'divider', sourceRoot: 'input' },
      ],
      dependencies: [],
    },
    {
      id          : '3',
      handler_id  : 'sum',
      input       : [
        { key: 'value1', sourceKey: '1.result', sourceRoot: 'scope' },
        { key: 'value2', sourceKey: '2.result', sourceRoot: 'scope' },
      ],
      dependencies: ['1', '2'],
    },
  ]
};

const inputMatch: IDataConvertModel = [
  { from: 'x', to: 'x' },
  { from: 'y', to: 'y' },
  { to: 'divider', defaultValue: 2 },
];

const outputMatch: IDataConvertModel = [
  { from: 'result', to: 'result' },
];

const input = {
  x: 10,
  y: 20,
};

buildChain(chain, handlers)(inputMatch, outputMatch, input).then(console.log); // { result: 110 }
```
