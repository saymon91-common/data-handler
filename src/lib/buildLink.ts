import * as util from 'util';

import { converter } from '@prof-itgroup/data-converter/lib/converter';
import { factory } from '@prof-itgroup/data-converter/lib/factory';
import { IDataConvertModel, INode as IConvertNode } from '@prof-itgroup/data-converter/lib/types';

import { IHandler, IHandlerConfig, ILinkConfig, ISources } from './types';

import { AsyncFunction } from './AsyncFunction';

export const buildLink = <I, O>(linkCfg: ILinkConfig, handlerCfg: IHandlerConfig): IHandler => {
  const func: (data: I) => O = new AsyncFunction('data', handlerCfg.code).bind({ util });

  const inputCfg: IDataConvertModel = handlerCfg.input.map<IConvertNode>(hInputItem => {
    const lInputCfg = linkCfg.input.find(i => i.key === hInputItem.key);

    if (hInputItem.required && !lInputCfg) {
      throw new Error('Bad link config');
    }

    return {
      defaultValue: hInputItem.defaultValue,
      from: lInputCfg && `${lInputCfg.sourceRoot}.${lInputCfg.sourceKey}`,
      to: hInputItem.key,
    };
  });

  const outputCfg: IDataConvertModel = handlerCfg.output.map<IConvertNode>(hOutputItem => ({
    defaultValue: hOutputItem.defaultValue,
    from: hOutputItem.key,
    to: `${linkCfg.id}.${hOutputItem.key}`,
  }));

  const buildInput = factory(converter, inputCfg);
  const buildOutput = factory(converter, outputCfg);

  return {
    id: linkCfg.id,
    run: async (data: ISources) => buildOutput(await func(buildInput(data))),
  };
};
