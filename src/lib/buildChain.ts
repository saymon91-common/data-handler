import { converter } from '@prof-itgroup/data-converter/lib/converter';
import { factory } from '@prof-itgroup/data-converter/lib/factory';
import { IDataConvertModel, INode as IConvertNode } from '@prof-itgroup/data-converter/lib/types';

import { Chain, IChainConfig, IHandlerConfig } from './types';

import { buildLinks } from './buildLinks';

export const buildChain = <I, O>(cfg: IChainConfig, handlers: IHandlerConfig[]): Chain<I, O> => {
  const links = buildLinks(cfg.links, handlers);

  const outputCfg: IDataConvertModel = cfg.output.map<IConvertNode>(field => ({
    defaultValue: field.defaultValue,
    from: field.sourceKey,
    to: field.key,
  }));

  const outputConverter = factory(converter, outputCfg);
  return async (inputMatch: IDataConvertModel, outputMatch: IDataConvertModel, input: any) => {
    const sources = { input: converter(inputMatch, input), scope: {} };
    for (const { run } of links) {
      const res = await run(sources);
      sources.scope = { ...sources.scope, ...res };
    }

    return converter(outputMatch, outputConverter(sources.scope));
  };
};
