import { IDataConvertModel } from '@prof-itgroup/data-converter/lib/types';

export type Nullable<T> = T | null;

export type DataTypes = string | number | boolean;

export interface IHandlerField<T extends Nullable<DataTypes> = DataTypes> {
  key: string;
  required: boolean;
  defaultValue?: T;
  nullable: boolean;
}

export interface IHandlerConfig {
  id: string;
  name: string;
  input: IHandlerField[];
  output: IHandlerField[];
  code: string;
}

export interface ISources {
  input: any;
  scope: any;
}

export interface ILinkField {
  key: string;
  sourceKey: string;
  sourceRoot: keyof ISources;
}

export interface ILinkConfig<H extends IHandlerConfig = any> {
  id: string;
  handler_id: string;
  dependencies: string[];
  input: ILinkField[];
}

export interface IChainInputField<T extends Nullable<DataTypes> = DataTypes> {
  key: string;
  sourceKey?: string;
  nullable: boolean;
  defaultValue?: T;
  required: boolean;
}

export interface IChainOutputField<T extends Nullable<DataTypes> = DataTypes> {
  key: string;
  sourceKey?: string;
  nullable: boolean;
  defaultValue?: T;
}

export interface IChainConfig {
  id: string;
  name: string;
  input: IChainInputField[];
  output: IChainOutputField[];
  links: ILinkConfig[];
}

export interface IHandler {
  id: string;

  run(data: ISources): PromiseLike<any>;
}

export type Chain<I, O> = (inputMatch: IDataConvertModel, outputMatch: IDataConvertModel, data: I) => PromiseLike<O>;
