import { IHandler, IHandlerConfig, ILinkConfig } from './types';

import { buildLink } from './buildLink';

export const buildLinks = (links: ILinkConfig[], handlers: IHandlerConfig[], built: IHandler[] = []): IHandler[] => {
  if (!links.length) {
    return built;
  }

  const linkCfgIdx = links.findIndex(cfg => cfg.dependencies.every(id => built.find(hdl => hdl.id === id)));

  if (linkCfgIdx === -1) {
    throw new Error('Bad links dependencies config');
  }

  const linkCfg = links[linkCfgIdx];

  const handlerCfg = handlers.find(cfg => cfg.id === linkCfg.handler_id);

  if (!handlerCfg) {
    throw new Error(`Handler [${linkCfg.handler_id}] is not found`);
  }

  built.push(buildLink(linkCfg, handlerCfg));
  links.splice(linkCfgIdx, 1);

  return buildLinks(links, handlers, built);
};
