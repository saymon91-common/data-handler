import { IDataConvertModel } from '@prof-itgroup/data-converter/lib/types';

import { IChainConfig, IHandlerConfig } from '../lib/types';

import { buildChain } from '..';

const handlers: { [key: string]: IHandlerConfig } = {
  normalizeLngLat: {
    id: 'normalizeLngLat',
    name: 'normalizeLngLat',
    input: [
      { key: 'lat', required: true, defaultValue: undefined, nullable: false },
      { key: 'lon', required: true, defaultValue: undefined, nullable: false },
      { key: 'alt', required: false, defaultValue: 0, nullable: false },
      { key: 'maxAlt', required: true, defaultValue: 0, nullable: false },
    ],
    output: [
      { key: 'lat', required: true, defaultValue: undefined, nullable: false },
      { key: 'lon', required: true, defaultValue: undefined, nullable: false },
      { key: 'alt', required: false, defaultValue: 0, nullable: false },
    ],
    code: `
      const { lat, lon, alt, maxAlt } = data;
      return { lat: Math.max(-180, Math.min(lat, 180)), lon: Math.max(-90, Math.min(lon, 90)), alt: Math.max(-100, Math.min(maxAlt, alt)) }; 
    `,
  },
  toPercent: {
    id: 'toPercent',
    name: 'toPercent',
    input: [
      { key: 'value', required: false, defaultValue: 0, nullable: true },
      { key: 'full', required: true, defaultValue: undefined, nullable: false },
    ],
    output: [{ key: 'value', required: false, defaultValue: 0, nullable: true }],
    code: 'return { value: data.value / data.full * 100 };',
  },
  sum: {
    id: 'sum',
    name: 'sum',
    input: [
      { key: 'acc', required: false, defaultValue: 0, nullable: false },
      { key: 'value', required: true, defaultValue: undefined, nullable: true },
    ],
    output: [{ key: 'result', required: true, defaultValue: 0, nullable: true }],
    code: 'return { result: data.acc + data.value };',
  },
  format: {
    id: 'format',
    name: 'format',
    input: [
      { key: 'value', required: false, defaultValue: undefined, nullable: true },
      { key: 'template', required: true, defaultValue: undefined, nullable: false },
    ],
    output: [{ key: 'formatted', required: true, defaultValue: undefined, nullable: false }],
    code: ' return { formatted: this.util.format(data.template, data.value.toFixed(2)) };',
  },
};

const chain: IChainConfig = {
  id: 'test',
  name: 'test',
  input: [
    { key: 'position', required: true, nullable: false, defaultValue: undefined },
    { key: 'dut1', required: false, nullable: false, defaultValue: 0 },
    { key: 'dut2', required: false, nullable: false, defaultValue: 0 },
    { key: 'dut3', required: false, nullable: false, defaultValue: 0 },
    { key: 'dut1Full', required: false, nullable: false, defaultValue: 0 },
    { key: 'dut2Full', required: false, nullable: false, defaultValue: 0 },
    { key: 'dut3Full', required: false, nullable: false, defaultValue: 0 },
    { key: 'maxAlt', required: true, nullable: false, defaultValue: undefined },
    { key: 'percentTemplate', required: true, nullable: false, defaultValue: '%d%' },
  ],
  output: [
    { key: 'location.coordinates.0', sourceKey: '1.lat', nullable: false, defaultValue: undefined },
    { key: 'location.coordinates.1', sourceKey: '1.lon', nullable: false, defaultValue: undefined },
    { key: 'location.coordinates.2', sourceKey: '1.alt', nullable: false, defaultValue: undefined },
    { key: 'location.type', sourceKey: undefined, nullable: false, defaultValue: 'Point' },
    { key: 'fullFuelLevel', sourceKey: '4.result', nullable: false, defaultValue: 0 },
    { key: 'fullFuelPercentage', sourceKey: '11.value', nullable: false, defaultValue: 0 },
    { key: 'fullFuelPercentageFormatted', sourceKey: '12.formatted', nullable: false },
  ],
  links: [
    {
      id: '1',
      handler_id: 'normalizeLngLat',
      input: [
        { key: 'lat', sourceKey: 'position.coordinates.0', sourceRoot: 'input' },
        { key: 'lon', sourceKey: 'position.coordinates.1', sourceRoot: 'input' },
        { key: 'alt', sourceKey: 'position.coordinates.2', sourceRoot: 'input' },
        { key: 'maxAlt', sourceKey: 'maxAlt', sourceRoot: 'input' },
      ],
      dependencies: [],
    },
    {
      id: '2',
      handler_id: 'sum',
      input: [{ key: 'value', sourceKey: 'dut1', sourceRoot: 'input' }],
      dependencies: [],
    },
    {
      id: '3',
      handler_id: 'sum',
      input: [
        { key: 'acc', sourceKey: '2.result', sourceRoot: 'scope' },
        { key: 'value', sourceKey: 'dut2', sourceRoot: 'input' },
      ],
      dependencies: ['2'],
    },
    {
      id: '4',
      handler_id: 'sum',
      input: [
        { key: 'acc', sourceKey: '3.result', sourceRoot: 'scope' },
        { key: 'value', sourceKey: 'dut3', sourceRoot: 'input' },
      ],
      dependencies: ['3'],
    },
    {
      id: '5',
      handler_id: 'toPercent',
      input: [
        { key: 'full', sourceKey: 'dut1Full', sourceRoot: 'input' },
        { key: 'value', sourceKey: 'dut1', sourceRoot: 'input' },
      ],
      dependencies: [],
    },
    {
      id: '6',
      handler_id: 'toPercent',
      input: [
        { key: 'full', sourceKey: 'dut2Full', sourceRoot: 'input' },
        { key: 'value', sourceKey: 'dut2', sourceRoot: 'input' },
      ],
      dependencies: [],
    },
    {
      id: '7',
      handler_id: 'toPercent',
      input: [
        { key: 'full', sourceKey: 'dut3Full', sourceRoot: 'input' },
        { key: 'value', sourceKey: 'dut3', sourceRoot: 'input' },
      ],
      dependencies: [],
    },
    {
      id: '8',
      handler_id: 'sum',
      input: [{ key: 'value', sourceKey: 'dut1Full', sourceRoot: 'input' }],
      dependencies: ['5'],
    },
    {
      id: '9',
      handler_id: 'sum',
      input: [
        { key: 'acc', sourceKey: '8.result', sourceRoot: 'scope' },
        { key: 'value', sourceKey: 'dut2Full', sourceRoot: 'input' },
      ],
      dependencies: ['8'],
    },
    {
      id: '10',
      handler_id: 'sum',
      input: [
        { key: 'acc', sourceKey: '9.result', sourceRoot: 'scope' },
        { key: 'value', sourceKey: 'dut3Full', sourceRoot: 'input' },
      ],
      dependencies: ['9'],
    },
    {
      id: '11',
      handler_id: 'toPercent',
      input: [
        { key: 'full', sourceKey: '10.result', sourceRoot: 'scope' },
        { key: 'value', sourceKey: '4.result', sourceRoot: 'scope' },
      ],
      dependencies: ['4', '10'],
    },
    {
      id: '12',
      handler_id: 'format',
      input: [
        { key: 'value', sourceKey: '11.value', sourceRoot: 'scope' },
        { key: 'template', sourceKey: 'percentTemplate', sourceRoot: 'input' },
      ],
      dependencies: ['11'],
    },
  ],
};

const inputMatch: IDataConvertModel = [
  { from: 'pos.coordinates.0', to: 'position.coordinates.0' },
  { from: 'pos.coordinates.1', to: 'position.coordinates.1' },
  { to: 'dut1', from: 'fuel.0', defaultValue: 0 },
  { to: 'dut2', from: 'fuel.1', defaultValue: 0 },
  { to: 'dut3', defaultValue: 100 },
  { to: 'dut1Full', defaultValue: 140 },
  { to: 'dut2Full', defaultValue: 220 },
  { to: 'dut3Full', defaultValue: 100 },
  { to: 'maxAlt', defaultValue: 1000 },
  { to: 'percentTemplate', defaultValue: '%d%' },
];

const outputMatch: IDataConvertModel = [
  { from: 'location', to: 'position' },
  { from: 'fullFuelPercentageFormatted', to: 'fuelLevel' },
];

const input = {
  pos: {
    coordinates: [54.234345354, 334244324],
  },
  lat: 345456,
  lon: 456567,
  fuel: [120, 180],
};

const output = {
  fuelLevel: '86.96%',
  position: {
    coordinates: [54.234345354, 90, 0],
    type: 'Point',
  },
};

const res = buildChain(
  chain,
  Object.entries(handlers).map(([, handlerCfg]) => handlerCfg),
)(inputMatch, outputMatch, input);
test('HANDLE DATA', async () => expect(await res).toEqual(output));
